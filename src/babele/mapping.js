import { resolvePath } from 'path-value';

const defaultMappings = {
  Global: {
    name: 'name'
  },

  Actor: {
    description: 'data.details.biography.value'
  },

  Item: {
    description: 'data.description.value'
  },

  JournalEntry: {
    description: 'content'
  }
};

/**
 * Test if the passed value is a babele converter
 * @param {*} value
 * @returns {boolean} Boolean
 */
export function isConverter (value) {
  return value !== null && typeof value !== 'string' && typeof value === 'object' && resolvePath(value, 'path').exists;
}

/**
 * Merge the default and custom mappings and return a new one
 * @param {object} customMappings All mappings on this Object with the same name will overwrite the default mappings
 * @returns {object} New Object mappings
 */
export function getMapping (customMappings) {
  const mapping = {
    ...customMappings,
    ...defaultMappings,
    Global: { ...defaultMappings.Global, ...customMappings.Global },
    Actor: { ...defaultMappings.Actor, ...customMappings.Actor },
    Item: { ...defaultMappings.Item, ...customMappings.Item },
    JournalEntry: { ...defaultMappings.JournalEntry, ...customMappings.JournalEntry }
  };

  return mapping;
}

/**
 * Check if the mapping already exists, and if it's not a default mapping save it on the target Object
 * @param {string} type Entity Type
 * @param {string} key Object key
 * @param {string|object} value Object value (Could be a string or an Object in case of a converter)
 * @param {object} target Destination Object
 */
export function setMapping (type, key, value, target) {
  const defaultExists = resolvePath(defaultMappings, [type, key]).exists;
  const isDefault = defaultExists ? value === defaultMappings[type][key] : false;

  if (target.mapping && target.mapping[key]) {
    return;
  }

  if (defaultExists && isDefault) {
    return;
  }

  if (value?.default) {
    return;
  }

  target.mapping = target.mapping || {};
  target.mapping[key] = value;
}
