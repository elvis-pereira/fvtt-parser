import { mkdir, writeFile } from 'fs';
import { getMapping, setMapping, isConverter } from './mapping.js';
import { resolvePath } from 'path-value';

/**
 * Read a NDJSON file and parse it into a object
 * @param {string} ndjson
 * @returns {Object} return the parsed file as an Object
 * @throws {string} Error message
 */
export function toJson (ndjson) {
  return ndjson.toString().trim().split('\n').map(JSON.parse);
}

/**
 * Read an Object containing items and save on a new Object following babele style
 * @param {object} items unformatted list of items
 * @param {object} target destination Object of formatted items
 * @param {boolean} useName boolean value telling if the name value should be used as id
 */
export function addItems (items, target, useName = false) {
  for (const item in items) {
    const itemId = useName ? resolvePath(items, [item, 'name']) : resolvePath(items, [item, '_id']);
    const itemName = resolvePath(items, [item, 'name']);
    const itemDescription = resolvePath(items, [item, 'data', 'description', 'value']);

    if (itemId.exists && itemName.exists && itemName.value !== '') {
      // Create items object if they don't exists yet
      target.items = target.items || {};
      target.items[itemId.value] = target.items[itemId.value] || {};

      target.items[itemId.value].name = itemName.value;

      if (itemDescription.exists && itemDescription.value !== '') {
        target.items[itemId.value].description = itemDescription.value;
      }
    }
  }
}

/**
 * Create a babele formatted Object containing text to be translated
 * @param {object} rawData unformatted data to be parsed
 * @param {string} label string containing the display name of the pack
 * @param {object} customMapping custom mapping to be used and tested to create translation
 * @param {boolean} useName boolean value telling if the name value should be used as id
 * @returns {object} Babele formatted Object
 */
export function createTranslation (rawData, label, customMapping = {}, useName = false, includeItems = true) {
  const babeleTranslation = {
    label: label,
    entries: {}
  };

  const unsortedEntries = {};

  const mapping = getMapping(customMapping);

  Object.values(rawData).forEach(entry => {
    const entryId = useName ? entry.name : entry._id;
    const items = resolvePath(entry, 'items');

    for (const entityType in mapping) {
      for (const [key, value] of Object.entries(mapping[entityType])) {
        if (isConverter(value)) {
          const res = resolvePath(entry, value.path);
          if (res.exists && res.value !== '') {
            setMapping(entityType, key, value, babeleTranslation);
          }
          if ((value?.includeValue || value?.default) && res.value) {
            unsortedEntries[entryId] = unsortedEntries[entryId] || {};
            unsortedEntries[entryId][key] = res.value;
          }
          if (items.exists && includeItems) {
            unsortedEntries[entryId] = unsortedEntries[entryId] || {};
            addItems(items.value, unsortedEntries[entryId], useName);
          }
        } else if (!isConverter(value)) {
          const res = resolvePath(entry, value);
          if (res.exists && res.value !== '') {
            setMapping(entityType, key, value, babeleTranslation);
            unsortedEntries[entryId] = unsortedEntries[entryId] || {};
            unsortedEntries[entryId][key] = res.value;

            if (items.exists && includeItems) {
              addItems(items.value, unsortedEntries[entryId], useName);
            }
          }
        }
      }
    }
  });

  Object.keys(unsortedEntries).sort().forEach(function (key) {
    babeleTranslation.entries[key] = unsortedEntries[key];
  });

  if (babeleTranslation?.mapping) {
    Object.entries(babeleTranslation.mapping).map(entry => delete entry[1].includeValue);
  }

  return babeleTranslation;
}

/**
 * Save translation to disk
 * @param {object} data Object that should be saved to disk
 * @param {string} destinationPath string containing the destination path
 * @param {string} fileName string containing the final file name
 */
export function saveFile (data, destinationPath, fileName) {
  mkdir(destinationPath, { recursive: true }, (error) => {
    if (error) throw error;
  });

  writeFile(`${destinationPath}/${fileName}`, JSON.stringify(data, null, 2), (error) => {
    if (error) throw error;
    console.log(`${fileName} saved to ${destinationPath}`);
  });
}
